import json
import joblib
import mwapi
import numpy as np
import pandas as pd
import re

from dataclasses import dataclass
from flask import abort, Flask, jsonify, Response
from functools import lru_cache
from typing import Dict, List, Optional
from sentence_transformers import SentenceTransformer
from werkzeug.routing import BaseConverter
from xgboost import XGBClassifier


@dataclass
class Classification:
    prediction: bool
    probability: float


@dataclass
class Revision:
    comment: str
    descriptions: Dict[str, Dict[str, str]]
    parent_id: int
    tags: List[str]


@dataclass
class Summary:
    edit_type: str
    language: str
    description: str


class RevisionIdsConverter(BaseConverter):
    regex = r"\d+(?:\|\d+)*"

    def to_python(self, value):
        return [int(x) for x in value.split("|")]

    def to_url(self, value):
        return "|".join(str(x) for x in value)


app = Flask(__name__)
app.url_map.converters["revision_ids"] = RevisionIdsConverter


USER_AGENT = "description edit scoring tool"
SUMMARY_RE = re.compile(
    r"\/\* (wbsetdescription-add|wbsetdescription-set):1\|([\w-]+) \*\/ (.+)",
    flags=re.UNICODE,
)


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


@app.errorhandler(400)
def bad_request(e):
    return jsonify(error=str(e)), 400


@app.errorhandler(500)
def internal_server_error(e):
    return jsonify(error=str(e)), 500


@app.route("/api/v1/<revision_ids:revision_ids>")
def score_revisions(revision_ids: List[int]) -> Response:
    if len(revision_ids) > 20:
        abort(400, "Number of revision ids exceeds 20.")
    return jsonify({"results": [generate_revision_score(id) for id in revision_ids]})


def generate_revision_score(revision_id: int) -> Dict:
    session = mwapi.Session(host="https://www.wikidata.org", user_agent=USER_AGENT)
    revision = get_revision(session, revision_id)
    if not revision:
        return make_result(
            revision_id, 400, error=f"Unable to find a revision with id {revision_id}"
        )
    if "wikidata-ui" not in revision.tags:
        return make_result(
            revision_id,
            400,
            error=f"Revision {revision_id} does not contain tag 'wikidata-ui'",
        )

    revision_summary = extract_summary(revision.comment)
    if not revision_summary:
        return make_result(
            revision_id, 400, error=f"Revision {revision_id} is not a description edit"
        )

    previous_revision = get_revision(session, revision.parent_id)
    if not previous_revision:
        return make_result(
            revision_id,
            400,
            error=f"Unable to find a parent revision for revision {revision_id}",
        )
    if not previous_revision.descriptions:
        return make_result(revision_id, 400, error="Insufficient data for scoring")

    encoder = get_encoder()
    descriptions = [d["value"] for d in previous_revision.descriptions.values()]

    # encode descriptions and get their centroid
    description_embeddings = encoder.encode(descriptions)
    centroid = calculate_centroid(description_embeddings)

    # calculate the distance between the new description and the centroid
    new_description_embedding = encoder.encode(revision_summary.description)
    distance = squared_distance(new_description_embedding, centroid)

    # get probability of edit being 'damaging' using pre-trained model
    model = get_model()
    result = classify(model, distance)
    return make_result(
        revision_id, 200, prediction=result.prediction, probability=result.probability
    )


def make_result(revision_id: int, status: int, **kwargs) -> Dict:
    return {"revision_id": revision_id, "status": status, **kwargs}


@lru_cache(maxsize=None)
def get_encoder() -> SentenceTransformer:
    return SentenceTransformer(
        "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2"
    )


@lru_cache(maxsize=None)
def get_model() -> XGBClassifier:
    return joblib.load("models/xgb_classifier.sav")


def get_revision(session: mwapi.Session, revision_id: int) -> Optional[Revision]:
    response = session.get(
        action="query",
        format="json",
        formatversion=2,
        prop="revisions",
        revids=revision_id,
        rvprop="ids|content|comment|tags",
        rvslots="main",
    )
    try:
        rev_info = response["query"]["pages"][0]["revisions"][0]
        rev_content = json.loads(rev_info["slots"]["main"]["content"])
        return Revision(
            comment=rev_info["comment"],
            descriptions=rev_content["descriptions"] or {},
            parent_id=rev_info["parentid"],
            tags=rev_info["tags"],
        )
    except KeyError:
        return None


def calculate_centroid(vectors: np.ndarray) -> np.ndarray:
    sum_vec = np.add.reduce(vectors)
    return np.divide(sum_vec, len(vectors))


def squared_distance(v1: np.ndarray, v2: np.ndarray) -> float:
    return float(np.linalg.norm(v1 - v2))


def extract_summary(comment: str) -> Optional[Summary]:
    match = SUMMARY_RE.fullmatch(comment)
    if not match:
        return None
    return Summary(
        edit_type=match.group(1), language=match.group(2), description=match.group(3)
    )


def classify(model: XGBClassifier, distance: float) -> Classification:
    X = pd.DataFrame([{"distance": distance}])
    [pred] = model.predict(X)
    [[_prob_no, prob_yes]] = model.predict_proba(X)
    return Classification(prediction=bool(pred), probability=float(prob_yes))
