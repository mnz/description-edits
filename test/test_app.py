import json
import mwapi
import numpy as np
import os
import pytest

from app import (
    calculate_centroid,
    classify,
    extract_summary,
    get_model,
    get_revision,
    Revision,
    squared_distance,
    Summary,
)
from numpy.testing import assert_allclose
from typing import Dict, Optional


@pytest.fixture
def mock_session(mocker):
    return mocker.patch.object(mwapi, "Session", autospec=True)


class TestGetRevision:
    """Test app.get_revision()"""

    def response(self, filename: str) -> Dict:
        current_dir = os.path.dirname(__file__)
        responses_dir = os.path.join(current_dir, "responses")
        response_filename = os.path.join(responses_dir, filename)

        with open(response_filename) as f:
            return json.load(f)

    def test_with_set_description(
        self,
        mock_session,
    ) -> None:

        revision_id = 1544176149
        mock_session.get.return_value = self.response("set_description.json")
        expected = Revision(
            comment="/* wbsetdescription-add:1|ja */ 子に公昌",
            descriptions={
                "ja": {"language": "ja", "value": "子に公昌"},
            },
            parent_id=1544173964,
            tags=["wikidata-ui"],
        )
        revision = get_revision(mock_session, revision_id)
        assert revision == expected

    def test_with_set_claim(
        self,
        mock_session,
    ) -> None:
        revision_id = 1544173964
        mock_session.get.return_value = self.response("set_claim.json")
        expected = Revision(
            comment="/* wbsetclaim-create:2||1 */ [[Property:P40]]: [[Q110125409]]",
            descriptions={},
            parent_id=1544172042,
            tags=["wikidata-ui"],
        )
        revision = get_revision(mock_session, revision_id)
        assert revision == expected

    def test_with_bad_id(
        self,
        mock_session,
    ) -> None:
        revision_id = 43262857400
        mock_session.get.return_value = self.response("bad_id.json")
        revision = get_revision(mock_session, revision_id)
        assert revision is None


def test_calculate_centroid() -> None:
    vectors = np.array([[0.1, 0.5, 0.3], [0.3, 0.5, 0.7]], np.float32)
    centroid = calculate_centroid(vectors)
    assert_allclose(centroid, np.array([0.2, 0.5, 0.5], np.float32))


@pytest.mark.parametrize(
    "vector1,vector2,expected",
    [
        (
            np.array([0.4, 0.2, 0.5], np.float32),
            np.array([0.1, -0.1, 0.33], np.float32),
            0.457,
        ),
        (
            np.array([0.1, -0.1, 0.33], np.float32),
            np.array([0.4, 0.2, 0.5], np.float32),
            0.457,
        ),
    ],
)
def test_squared_distance(
    vector1: np.ndarray, vector2: np.ndarray, expected: float
) -> None:
    distance = squared_distance(vector1, vector2)
    assert distance == pytest.approx(expected, rel=1e-3)


@pytest.mark.parametrize(
    "example_comment,expected",
    [
        (
            "/* wbsetdescription-add:1|sl */ slovenska avtohtona pasma ovce",
            Summary(
                edit_type="wbsetdescription-add",
                language="sl",
                description="slovenska avtohtona pasma ovce",
            ),
        ),
        (
            "/* wbsetdescription-set:1|or */ ଉଇକିମିଡ଼ିଆ ଶ୍ରେଣୀ",
            Summary(
                edit_type="wbsetdescription-set",
                language="or",
                description="ଉଇକିମିଡ଼ିଆ ଶ୍ରେଣୀ",
            ),
        ),
        (
            "/* wbsetdescription-set:1|zh-hant */ 中華人民共和國的首都",
            Summary(
                edit_type="wbsetdescription-set",
                language="zh-hant",
                description="中華人民共和國的首都",
            ),
        ),
        ("/* wbsetclaim-create:2||1 */ [[Property:P40]]: [[Q110125409]]", None),
    ],
)
def test_extract_summary(
    example_comment: str, expected: Optional[Summary]
) -> None:
    summary = extract_summary(example_comment)
    assert summary == expected


def test_classify() -> None:
    model = get_model()

    result = classify(model, 15.0)
    assert result.prediction is True
    assert result.probability >= 0.8

    result = classify(model, 3.21)
    assert result.prediction is False
    assert result.probability <= 0.3
